from pyb import I2C, delay

class AHT20:
    dev_addr = 0x38
    cal_mask = 0b00001000
    cal_command = bytes([0xbe, 0x08, 0x00])  # calibration command
    busy_mask = 0b10000000
    mes_command = bytes([0xAC, 0x33, 0x00])  # trigger measurment command
    humidity_mask = 0b11110000
    temperature_mask = 0b00001111

    def __init__(self, i2c_id=1):
        self.i2c = I2C(i2c_id, I2C.MASTER)  # configure as master

    def init(self):
        delay(40)  # wait 40 ms after power-on
        rceived_status_byte = self.i2c.recv(1, self.dev_addr)  # read status byte
        if (rceived_status_byte[0] & self.cal_mask) == 0:  # if not calibrated
            self.i2c.send(self.cal_command, self.dev_addr)  # send calibration command
            delay(10)  # wait 10 ms for calibration

    def _send_trigger_measurment(self):
        self.i2c.send(self.mes_command, self.dev_addr)
        delay(80)  # wait 80 ms for perform measurment
        return self.i2c.recv(7, self.dev_addr)  # read data

    def read_data(self):
        rceived_data_bytes = self._send_trigger_measurment()
        while (rceived_data_bytes[0] & self.busy_mask) != 0:  # if still in mesaurment
            rceived_data_bytes = self._send_trigger_measurment()

        humidity_raw = (int(rceived_data_bytes[1]) << 12) + (int(rceived_data_bytes[2]) << 4) + \
            (int(rceived_data_bytes[3] & self.humidity_mask) >> 4)
        humidity = (humidity_raw / (2**20)) * 100

        temperature_raw = (int(rceived_data_bytes[3] & self.temperature_mask) << 16) + \
            (int(rceived_data_bytes[4]) << 8) + (int(rceived_data_bytes[5]))
        temperature = (temperature_raw / (2**20)) * 200 - 50

        return(humidity, temperature)
