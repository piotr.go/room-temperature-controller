from pyb import Pin, LED, UART, Switch, Timer, delay, disable_irq, enable_irq
from temp_hum_AHT20 import AHT20
from OLED_SSD1331 import SSD1331


program_mode = False
screen_mode = True
mode_counter = 0
mode_counter_program = 3
mode_counter_screen = 5
button_flag = 0  # to avoid debouncing

temperature_sp = 22*2  # doubled to avoid foloat in callback
temperature_sp_min = 18*2  # doubled to avoid foloat in callback
temperature_sp_max = 24*2  # doubled to avoid foloat in callback
temperature_sp_lag = 0.5


def timer_callback(timer):  # for debouncing purpose
    global button_flag, temperature_sp, screen_mode
    timer.deinit()
    if sw.value() and (screen_mode == False):  # switch on screen
        screen_mode = True
    elif sw.value() and program_mode:
        temperature_sp += 1
        if temperature_sp > temperature_sp_max:
            temperature_sp = temperature_sp_min
    button_flag = 0


def button_callback():
    global button_flag
    if (button_flag == 0):  # to avoid debouncing
        button_flag = 1
        tim.init(freq=50, callback=timer_callback)


def set_valve(status):
    valve_pin.value(not(status))  # Low on pin set on rely 


led_blue = LED(4)
led_blue.on()

uart = UART(2, baudrate=9600, bits=8, parity=None, stop=1)  # init with given baudrate

aht20 = AHT20(i2c_id=1)
aht20.init()

ssd1331 = SSD1331(spi_id=1, cs='PC5', dc='PB1', res='PE7')
ssd1331.init()

tim = Timer(1, freq=50, callback=timer_callback)
sw = Switch()
sw.callback(button_callback)

valve_pin = Pin('PB7', mode=Pin.OUT_PP, pull=Pin.PULL_UP)
set_valve(0)

getting_warmer = True

uart_msg_text = ''
oled_hum_text = ''
oled_temp_text = ''
oled_temp_sp_text = ''

# main program loop
while True:
    irq_state = disable_irq()  # Start of critical section (disable interruptions)
    
    if program_mode:
        if screen_mode:
            # display temperature set point
            ssd1331.display_text(0, 16, oled_temp_sp_text, 'black')
            oled_temp_sp_text = '{:.2f} C'.format(temperature_sp/2)
            ssd1331.display_text(0, 16, oled_temp_sp_text, 'red')
    else:
        # measure by sensor
        humidity, temperature = aht20.read_data()

        # controller with hysteresis
        if ((temperature_sp/2) - temperature > -temperature_sp_lag) and getting_warmer:
            set_valve(1)
        elif ((temperature_sp/2) - temperature <= temperature_sp_lag) and getting_warmer:
            set_valve(0)
            getting_warmer = False
        elif ((temperature_sp/2) - temperature <= temperature_sp_lag) and not(getting_warmer):
            set_valve(0)
        elif ((temperature_sp/2) - temperature > -temperature_sp_lag) and not(getting_warmer):
            set_valve(1)
            getting_warmer = True
        else:
            set_valve(0)

        # send message via UART
        uart_msg_text = "H = {:.2f}% | T = {:.2f} C\r\n".format(humidity, temperature)
        uart.write(uart_msg_text)

        # display on OLED screen
        if screen_mode:
            ssd1331.display_text(0, 0, oled_hum_text, 'black')
            oled_hum_text = "H = {:.2f} %".format(humidity)
            ssd1331.display_text(0, 0, oled_hum_text, 'blue')
            ssd1331.display_text(0, 16, oled_temp_text, 'black')
            oled_temp_text = "T = {:.2f} C".format(temperature)
            ssd1331.display_text(0, 16, oled_temp_text, 'red')

    # check if button is pushed
    if sw.value():
        mode_counter += 1
    else:
        mode_counter = 0

    if (mode_counter == mode_counter_program) and screen_mode:  # get in/out programmable mode 
        program_mode = not(program_mode)
        if program_mode:
            ssd1331.display_text(0, 0, oled_hum_text, 'black')
            ssd1331.display_text(0, 16, oled_temp_text, 'black')
            ssd1331.display_text(0, 0, 'Temp sp =', 'red')
            ssd1331.display_text(0, 16, oled_temp_sp_text, 'red')
        else:
            ssd1331.display_text(0, 0, 'Temp sp =', 'black')
            ssd1331.display_text(0, 16, oled_temp_sp_text, 'black')
            ssd1331.display_text(0, 0, oled_hum_text, 'blue')
            ssd1331.display_text(0, 16, oled_temp_text, 'red')
    elif mode_counter == mode_counter_screen:  # switch off screen
        screen_mode = False
        program_mode = False
        ssd1331.clear_screen('black')

    led_blue.toggle()
    
    enable_irq(irq_state)  # End of critical section (enable interruptions)

    delay(1000)
